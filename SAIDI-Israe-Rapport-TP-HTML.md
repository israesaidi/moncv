## TP HTML - UE Développement Clients Web - Dosi 2023-2024

### 1. Structure HTML
##### 1.1 Un CV structuré (mais un peu moche)
###### 1.1.1 Créer la page html nommée ubo-resume.html et contenant l'exemple de page HTML ci-dessus.
```html
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Mon CV</title>
    <meta name="description" content="A propos de moi">
    <meta name="keywords" content="web,developer,resume"/>
    <meta name ="author" content="Israe Saidi">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
    <div id="wrapper"></div>
</body>
</html>
```
###### 1.1.2 Ajouter un bloc header contenant : votre nom dans un bloc h1, une activité dans un bloc h2, unephoto (fictive si vous n’en avez pas), des informations complémentaires (par exemple : nom,prénom, adresse, date de naissance/âge, email, nationalité, compte twitter, google+, ...)
```html
    <header>
        <h1>Israe SAIDI</h1>
        <h2>Élève ingénieure logicielle</h2>
        <img src="./maPhoto.png" alt="Ma photo">
        <p>Nom: SAIDI</p>
        <p>Prénom: Israe</p>
        <p>Adresse: 2 rue des Archives cité universitaire de Kergoat</p>
        <p>Date de naissance: 29/12/2000</p>
        <p>Email: israe.2001.saidi@gmail.com</p>
        <p>Numéro de téléphone: +33 7 58 50 54 38</p>
        <p>Nationalité: Marocaine</p>
        <a>Compte linkedIn: </a><a href="https://www.linkedin.com/in/israe-saidi-685001203/">Israe SAIDI</a>
    </header>
```
###### 1.1.3 Ajouter des sections pour le contenu de votre CV par exemple un texte de présentation, une section expériences professionnelles (penser à <article>), la liste de vos compétences (penser à<dl><dt><dd>), une section formation, langues, hobbies, etc.
```html
    <section>
        <h3>Présentaion</h3>
        <p>Je suis une étudiante en double diplômation en ingénierie logicielle passionnée par les technologies de
            l'information, actuellement en quête de connaissances et d'expérience pour devenir une ingénieure logicielle
            compétente. Mon objectif est de contribuer au monde de l'informatique en développant des solutions
            innovantes et fiables.</p>
    </section>
    <section>
        <h3>Expériences Professionnelles</h3>
        <article>
            <h4>Société Vitsey à Tanger </h4>
            <p>2021 - 2022</p>
            <p>La mise en place d'une plateforme en ligne destinée à proposer des jeux de hasard personnalisés en
                réponse aux besoins des entreprises implique l'utilisation des technologies suivantes : Laravel pour la
                gestion du backend, JavaScript pour le développement de l'interface utilisateur, et MySQL comme base de
                données.</p>
        </article>
        <article>
            <h4>Stage interne à la faculté des Sciences et Techniques de Tanger </h4>
            <p>2020 - 2021</p>
            <p>La création d'une application mobile visant à surveiller et afficher l'état de santé des plantes
                d'intérieur en utilisant des capteurs IoT nécessite l'utilisation des outils suivants : React Native
                pour le développement de l'application mobile, PHP pour la gestion du backend, MySQL comme base de
                données, et Arduino pour l'intégration des capteurs IoT.</p>
        </article>
    </section>
    <section>
        <h3>Compétences Techniques</h3>
        <dl>
            <dt>Langages :</dt>
            <dd>C, C++, PHP, JavaScript, Java, C#,Solidity, Python</dd>
            <dt>Frameworks :</dt>
            <dd>Laravel, React js, Angular,Spring/SpringBoot, JEE, .Net Desktop,flask</dd>
            <dt>Bases de données :</dt>
            <dd>SQL, PL/SQL, NOSQL / SGBD : MySql, MongoDB, Postgres,SQLite, Oracle</dd>
            <dt>Modélisation et conception :</dt>
            <dd>Merise, UML</dd>
            <dt>Business Intelligence :</dt>
            <dd>Data warehouse, Data mining, Power BI</dd>
            <dt>Outils de développement :</dt>
            <dd>Git/Github, Docker, Figma, Linux, Jira</dd>
          </dl>
    </section>

    <section>
        <h3>Formation</h3>
        <article>
            <h4>Diplôme Master 2 en développement logiciels et systèmes d’informations.</h4>
            <p>2023 - 2024</p>
            <p>Université de Bretagne Occidentale à Brest, France.</p>
        </article>
        <article>
            <h4>Diplôme Ingénieur d'état en Logiciels et Systèmes Intelligents.</h4>
            <p>2021 - 2024</p>
            <p>Université Abdelmalek Essaâdi Faculté des Sciences et Techniques de Tanger, Maroc.</p>
        </article>
        <article>
            <h4>Diplôme Licence en Génie informatique.</h4>
            <p>2020 - 2021</p>
            <p>Université Abdelmalek Essaâdi Faculté des Sciences et Techniques de Tanger, Maroc.</p>
        </article>
        <article>
            <h4>Diplôme d'études universitaires scientifiques et techniques</h4>
            <p>2018 - 2020</p>
            <p>Université Abdelmalek Essaâdi Faculté des Sciences et Techniques de Tanger, Maroc.</p>
        </article>
    </section>
    <section>
        <h3>Langues</h3>
        <ul>
            <li>Arabe : Langue maternelle</li>
            <li>Français : TCF C1</li>
            <li>Anglais : Intermédiaire</li>
        </ul>
    </section>
    <section>
        <h3>Hobbies</h3>
        <ul>
            <li>Voyage</li>
            <li>Cuisine</li>
            <li>Natation</li>
        </ul>
    </section>
```
###### 1.1.4 Ajouter un footer contenant des liens vers différents comptes que vous pouvez avoir Facebook,LinkedIn, Github, Twitter, Flickr, etc. Ainsi qu'un minimum d'informations sur la page (NB : pasd'icones pour le moment).
```html
 <footer>
        <ul>
            <li><a href="https://www.facebook.com/">Facebook</a></li>
            <li><a href="https://www.linkedin.com/in/israe-saidi-685001203/">LinkedIn</a></li>
            <li><a href="https://github.com/IsraeSaidi">GitHub</a></li>
            <li><a href="https://twitter.com/">Twitter</a></li>
        </ul>
        <p>Informations de contact : israe.2001.saidi@gmail.com</p>
    </footer>
```
###### 1.1.5 Tester l'affichage de la page dans un navigateur.
- C'est bien fait.

###### 1.1.6 Une fois le contenu terminé, ajouter ce fichier dans un nouveau dépôt git et tagger cette version avec le tag « Structure »
```shell
C:\Users\HP\Desktop\TP HTML,CSS>git init
Initialized empty Git repository in C:/Users/HP/Desktop/TP HTML,CSS/.git/

C:\Users\HP\Desktop\TP HTML,CSS>git add ubo-resume.html maPhoto.png

C:\Users\HP\Desktop\TP HTML,CSS>git commit -m "Structure"
[master (root-commit) 85e6a83] Structure
 2 files changed, 124 insertions(+)
 create mode 100644 maPhoto.png
 create mode 100644 ubo-resume.html

C:\Users\HP\Desktop\TP HTML,CSS>git branch -M main

C:\Users\HP\Desktop\TP HTML,CSS>git remote add origin https://github.com/IsraeSaidi/TP-HTML.git

C:\Users\HP\Desktop\TP HTML,CSS>git push -u origin main
Enumerating objects: 4, done.
Counting objects: 100% (4/4), done.
Delta compression using up to 8 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 226.98 KiB | 25.22 MiB/s, done.
Total 4 (delta 0), reused 0 (delta 0), pack-reused 0
To https://github.com/IsraeSaidi/TP-HTML.git
 * [new branch]      main -> main
branch 'main' set up to track 'origin/main'.
```
### 1. Un peu de style
###### 1.1 Ajouter un fichier css/resume.css.
```css
html, button, input, select, textarea {
    color: #444;
}

html{
    font-size: 1em;
    line-height: 1.4;
}
```
###### 1.2 Ajouter dans la balise <head> de votre page html le changement de ce fichier CSS.
```html
    <link rel="stylesheet" href="css/resume.css">
```
###### 1.3 Modifier le style CSS de resume.css de manière à obtenir :
- a. Un fond de page html sombre (par ex. #777)
```css
html{
    font-size: 1em;
    line-height: 1.4;
    background-color: #777;
}
```
- b. Pour le contenu <div id=wrapper> : une largeur maximale de 900px, une largeur minimale de 320px, centrer en milieu de page. NB: il est possible d'utiliser une marge "auto" à gauche et à droite pour centrer horizontalement.
```css
#wrapper{
    max-width: 900px;
    min-width: 320px;
    margin: 0 auto;
}
```
- c. Pour le header: une image alignée à droite avec un contour d'épaisseur 1px, couleur #ccc, bordure arrondie à 3px, fond blanc, un effet d'ombre tel qu'avec les paramètres 0 2px 4px rgba(0,0,0,0.2).
```css
header img {
    border: 1px solid #ccc ;
    border-radius: 3px;
    background-color: white;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
}
```
###### 1.4 la police d'icônes "font awesome" va être utilisée
- a. Ajouter dans le HTML le lien de cette police.
```html
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
```
- b. L'utilisation de cette police est basé sur les classes, pour tester son utilisation on utilise:
```html
<i class="fa fa-camera-retro"></i>  
```
###### 1.5 Pour changer de la police de caractères nous allons utiliser "Adobe Edge Web Fonts"
- a. J'ai utilisé Google font 
```
https://fonts.google.com/
```
- b. C'est déja fait.
- c. Selon les instructions du site je dois ajouter les lignes suivantes dans le fichier html.
```html
 <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu+Mono&display=swap" rel="stylesheet">
```
et ça dans le fichier css.
```css
h1{
    font-family: 'Ubuntu Mono', monospace;
}<?xml version="1.0" encoding="UTF-8" ?>
<log4j:configuration xmlns:log4j='http://jakarta.apache.org/log4j/'>
    <appender name="Default" class="org.apache.log4j.RollingFileAppender">
        <param name="File" value="c:/logs/fr.ubo.hello.log" />
        <param name="MaxFileSize" value="10000KB" />
        <param name="Append" value="true" />
        <layout class="org.apache.log4j.PatternLayout">
            <param name="ConversionPattern" value="%d %-5p %37c | %m%n" />
        </layout>
    </appender>
    <logger name="fr.ubo.hello">
        <level value="debug" />
    </logger>
    <root>
        <level value="error" />
        <appender-ref ref="Default" />
        <appender-ref ref="Console" />
    </root>
</log4j:configuration>
```
###### 1.6 Modifier le style du reste de votre page pour la rendre plus jolie à votre goût
```css
html, button, input, select, textarea {
    color: #444;
}

html{
    font-size: 1em;
    line-height: 1.4;
    background-color: #ffffff;
}

#wrapper{
    max-width: 900px;
    min-width: 320px;
    margin: 0 auto;
}
header img {
    width: 20%;
    border: 1px solid #ccc ;
    border-radius: 3px;
    background-color: white;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
    margin-bottom: 10px;
}
h1{
    font-family: 'Ubuntu Mono', monospace;
}

* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  i{
    margin-right: 8px;
  }
  
  body {
    font-family: 'Ubuntu Mono', monospace;
    background-color: #f9f9f9;
    margin: 0;
    padding: 0;
  }
  
  #wrapper {
    max-width: 800px;
    margin: 0 auto;
    background-color: #ffffff;
    box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
    border-radius: 10px;
    padding: 70px;

  }
  
  header {
    text-align: center;
  }
  
  header h1 {
    font-size: 36px;
    color:  #007BFF;
    margin-bottom: 10px;
  }
  
  header h2 {
    font-size: 18px;
    color: #555;
    margin-bottom: 20px;
  }
  
  section {
    margin: 20px 0;
  }
  
  section h3 {
    font-size: 24px;
    color: #007BFF;
    margin-bottom: 10px;
    position: relative;
  }
  
  section h3::before {
    content: '';
    position: absolute;
    left: 0;
    bottom: -5px;
    width: 50px;
    height: 2px;
    background-color: #007BFF;
  }
  
  article {
    margin: 10px 0;
  }
  
  a {
    text-decoration: none;
    color: #007BFF;
  }
  
  a:hover {
    text-decoration: underline;
  }

  dl {
    margin: 10px 0;
  }
  
  dt {
    font-weight: bold;
  }
  
  dd {
    margin-left: 20px;
  }
  
  footer {
    text-align: center;
    margin-top: 20px;
  }
  
  footer ul {
    list-style: none;
  }
  
  footer ul li {
    display: inline;
    margin-right: 10px;
  }
  
  footer a {
    color: #007BFF;
    font-size: 20px;
  }
  
```
7- Valider les modifications dans GIT et placer un tag "CSS"
```shell
PS C:\Users\HP\Desktop\TP HTML,CSS> git add .
PS C:\Users\HP\Desktop\TP HTML,CSS> git commit -m "CSS"
On branch main
Your branch is ahead of 'origin/main' by 1 commit.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
PS C:\Users\HP\Desktop\TP HTML,CSS> git push -u origin main
Enumerating objects: 8, done.
Counting objects: 100% (8/8), done.
Delta compression using up to 8 threads
Compressing objects: 100% (5/5), done.
Writing objects: 100% (6/6), 6.18 KiB | 3.09 MiB/s, done.
Total 6 (delta 1), reused 0 (delta 0), pack-reused 0
remote: Resolving deltas: 100% (1/1), completed with 1 local object.
To https://github.com/IsraeSaidi/TP-HTML.git
   85e6a83..56548d1  main -> main
branch 'main' set up to track 'origin/main'.
PS C:\Users\HP\Desktop\TP HTML,CSS> 
```

### 3. Mise en ligne
Créer le repertoire et push notre projet
```shell
PS C:\Users\HP\Desktop\TP HTML,CSS> git add .
PS C:\Users\HP\Desktop\TP HTML,CSS> git commit -m "first commit"
[main fd71565] first commit
 1 file changed, 1 insertion(+), 1 deletion(-)
PS C:\Users\HP\Desktop\TP HTML,CSS> git remote set-url origin https://gitlab.com/israesaidi/moncv
PS C:\Users\HP\Desktop\TP HTML,CSS> git push origin main
>>
warning: redirecting to https://gitlab.com/israesaidi/moncv.git/
Enumerating objects: 37, done.
Counting objects: 100% (37/37), done.
Delta compression using up to 8 threads
Compressing objects: 100% (35/35), done.
Writing objects: 100% (37/37), 236.21 KiB | 4.22 MiB/s, done.
Total 37 (delta 17), reused 0 (delta 0), pack-reused 0
To https://gitlab.com/israesaidi/moncv
 * [new branch]      main -> main
```
Le contenu du fichier .gitlab-ci.yml
```yml
pages:
  stage: deploy
  script:
    - mkdir .public
    - cp -r * .public
    - mv .public public
  artifacts:
    paths:
      - public
  only:
    - main
```
et après on push ce fichier

Voir les captures d'écran.
### Adaptatif ou responsive
1- Créer un fichier css/responsive.css contenant l'exemple ci-dessous
```css
@media only screen and (max-width: 740px){
    #wrapper{background: #f77;}
}

@media only screen and (max-width: 570px){
    #wrapper{background: #7f7;}
}

@media only screen and (max-width: 480px){
    #wrapper{background: #77f;}
}
```
2- Ajouter l'utilisation de ce fichier à votre page html et expliquer ce qui se passe lorsqu'on redimensionne la fenêtre de navigation 